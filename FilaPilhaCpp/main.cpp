#include <iostream>
#include "pilha.cpp"
#include "fila.cpp"

int main (int argc, char const* argv[]) {
   cout << "Pilha: " << "\n";
   Pilha <char> p;
   p.push('J');
   p.push('A');
   p.push('G');
   p.push('U');
   p.push('A');
   p.push('R');
   p.push('U');
   p.push('A');
   p.push('N');
   p.push('A');
   p.imprime();
   p.pop();
   p.imprime();
   p.pop();
   p.imprime();         
   cout << "Front: " << p.front() << "\n";
   
   cout << "\n";
   cout << "Fila: " << "\n";
   Fila <char> q;
   q.push('J');
   q.push('A');
   q.push('G');
   q.push('U');
   q.push('A');
   q.push('R');
   q.push('U');
   q.push('A');
   q.push('N');
   q.push('A');
   q.imprime();
   q.pop();
   q.imprime();
   q.pop();
   q.imprime(); 
   cout << "Front: " << q.front() << "\n";
   
   return 0;
}
