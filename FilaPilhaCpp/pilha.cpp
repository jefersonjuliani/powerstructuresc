#include <iostream>

using namespace std;

template <class A> 
class Pilha {
protected:
   struct no {
      A cel;
      struct no *prox;
   };
   
   no *cab;
   no *ult;
   
public:
   Pilha () {
      cab = new no;
      cab->prox = NULL;
      ult = cab;
   }
   
   // Insere Início
   void push (A n) {
      no *aux = new no;
      aux->cel = n;
      aux->prox = cab->prox;
      cab->prox = aux;
   }
   
   bool empty () {
      return (cab->prox == NULL);
   }
   
   // Remove início
   void pop (void) {
      no *aux = cab->prox;
      cab->prox = cab->prox->prox;
      delete aux;
   }
   
   A front (void) {
      return cab->prox->cel;  
   }
   // Imprime
   void imprime () {
      no *aux = cab->prox;
      while (aux) {
         cout << aux->cel << " ";
         aux = aux->prox;      
      }
      cout << "\n";
   }
};



